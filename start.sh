#!/bin/bash

set -eu

function update_settings() {
    echo "==> Overriding settings"
    gosu cloudron:cloudron sqlite3 /app/data/data/kuma.db "INSERT INTO setting (key, value, type) VALUES ('checkUpdate', 'false', 'general') ON CONFLICT DO UPDATE SET value='false';"
    gosu cloudron:cloudron sqlite3 /app/data/data/kuma.db "INSERT INTO setting (key, value, type) VALUES ('trustProxy', 'true', 'general') ON CONFLICT DO UPDATE SET value='true';"
}

mkdir -p /app/data/data /app/data/db

[[ ! -f /app/data/env ]] && echo -e "# Add custom environment variables in this file\n# https://github.com/louislam/uptime-kuma/wiki/Environment-Variables\n\n" > /app/data/env
source /app/data/env

echo "==> Removing sqlite backup files"
rm -rf /app/data/data/*bak*

chown -R cloudron:cloudron /app/data

# sqlite3 does not allow multiple writes in WAL mode. This means that we cannot update settings in the background with uptime kuma running
# https://www.sqlite.org/wal.html (concurrency section, 3rd para)
if [[ ! -f "/app/data/data/kuma.db" ]]; then
    echo "==> Creating database on first run"
    /usr/local/bin/gosu cloudron:cloudron npm run start-server &
    pid=$!
    while ! curl --fail http://localhost:3001; do
        echo "=> Waiting for app to complete database initialization"
        sleep 10
    done
    echo "==> Stopping after database creating"
    kill -SIGTERM ${pid} # this kills the process group
fi

update_settings

echo "==> Starting Uptime Kuma"
exec /usr/local/bin/gosu cloudron:cloudron npm run start-server
