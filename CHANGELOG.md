[0.1.0]
- Initial commit

[0.2.0]
- Cleanup manifest and publish first unstable release

[0.3.0]
- Update Uptime Kuma to 1.0.6

[0.4.0]
- Update Uptime Kuma to 1.0.7
* View Certificate Info for HTTPS Website (Thanks @chakflying)
* Add maximum retries before the service is marked as down and a notification is sent #86 (Thanks @Spiritreader)

[0.5.0]
* Add ping capability

[0.6.0]
* Update Uptime Kuma to 1.0.10
* ignore TLS error for HTTPS websites
* add "Upside Down Mode"
* able to disable auth
* add support for LunaSea notifications (Thanks @NiNiyas)
* use font-awesome instead of emoji

[1.0.0]
* Update Uptime Kuma to 1.1.0
* Dark mode
* Accepted Status Codes for HTTP Monitor (Thanks @chakflying)
* Follow 3XX Redirects (Thanks @chakflying)
* Reset Password via CLI
* Pushy notification (Thanks @philippdormann)
* Search engine visibility (Allow or not allow indexing)
* You could now set the Discord bot name (Thanks @NiNiyas)
* Use font awesome icon
* Security updates

[1.1.0]
* Update Uptime Kuma to 1.2.0
* Add Ping Graph (Thanks @chakflying)
* Better IPv6 supports
* Pushbullet notification (Thanks @ChrisTheBaron)
* Octopush notification (Thanks @AlexandreGagner)

[1.2.0]
* Update Uptime Kuma to 1.3.2
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.3.2)
* Avoid unexpected high ping and some DNS problems.
* Add more monitor list styles, ping chart improvements and some mobile optimizations (Thanks @Ponkhy @chakflying)
* Add page change animation

[1.3.0]
* Update Uptime Kuma to 1.5.0
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.5.0)
* New Monitor Type: DNS
* Add mattermost notification (Thanks @rashad)
* Add rocket.chat notification (Thanks @antiseptikk)
* Add K8s template (Thanks @ClemontX, @srgvg)
* A lot of frontend enhancements (charts, css and mobile etc.) (Thanks @chakflying, @Ponkhy, @henrygd)
* Use less RAM (around 10-20 MB) (Thanks @duzun)
* Ping timeout from 2s to 10s

[1.3.1]
* Update Uptime Kuma to 1.5.2
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.5.2)
* Revert back to node-sqlite3, as better-sqlite3 causes a lot of troubles while installing

[1.3.2]
* Update Uptime Kuma to 1.5.3
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.5.3)
* Add Languages:
  * Danish (Thanks @Lrss)
  * Dutch (Thanks @xoniq)
  * Japanese (Thanks @kometchtech)
  * Korean (Thanks @dhfhfk)
  * Russian (Thanks @DX37)
  * Serbian Cyrillic (Thanks @dusansimic)
  * Serbian Latin (Thanks @dusansimic)
  * Simplified Chinese (Thanks @xinac721)
  * Spanish (Thanks @aictur)
  * Swedish (Thanks @LeviSnoot)

[1.4.0]
* Update Uptime Kuma to 1.6.1
* Backup/Restore all monitors and all notifications (Thanks @Ponkhy)
* Performance improvements (Faster list loading and better uptime calculation)
* Able to set default notifications (Thanks @Ponkhy)
* Add Languages:
  * Estonian (Thanks @jtagcat)
  * Italian (Thanks @iomataani)
  * Polish (Thanks @Misly16 @Saibamen)
  * Swedish (Thanks @LeviSnoot)
* Add more smtp config: cc, bcc, multiple addresses, sender name and ignore tls error
* A lot of UI/UX changes such as a better setup flow and hide/show button for sensitive data (Thanks @Ponkhy @Saibamen @tgxn)

[1.4.1]
* Update Uptime Kuma to 1.6.2
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.6.2)

[1.4.2]
* Update Uptime Kuma to 1.6.3
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.6.3)

[1.5.0]
* Update Uptime Kuma to 1.7.0
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.7.0)
* Status page
* Two-Factor Authentication (2FA), supports authenticators such as Authy, Google Authenticator (Thanks @Ponkhy)
* Add tags to monitors and searching (Thanks @chakflying)
* Retry interval (Thanks @No0Vad)
* Add notification: Microsoft Teams (Thanks @WillianRod)

[1.5.1]
* Update Uptime Kuma to 1.7.1
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.7.1)
* Change /status-page to /status
* Add Language: Bulgarian (Thanks @MrEddX)

[1.5.2]
* Update Uptime Kuma to 1.7.3
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.7.3)
* Fix retries problem (#506)
* Fix installation problem for non-Docker users (vite.js 2.6.x broke the installation)

[1.6.0]
* Update Uptime Kuma to 1.8.0
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.8.0)
* New Monitor Type - Push Type #279
* [Certificate] Display the full certificate chain (Thanks @chakflying) #519
* Use browser language as default language (Thanks @kvpt)
* [Notification] PromoSMS (Thanks @NixNotCastey)
* [Notification] Matrix (Thanks @Empty2k12)
* [Notification] Legacy Octopush (Thanks @DeeJayPee)
* [Language] Bulgarian (Thanks @MrEddX)
* [Language] Hungarian (Thanks @csabibela)
* [Language] Farsi, also add RTL language support (Thanks @cmandesign)
* [Docker] Slim Debian image size! Compressed size from 250MB to 100MB
* [Fix] Allow hostname with underscore (Thanks @chakflying)
* [Fix] SMTP - To Email is no longer required if CC or BCC is presented
* [Fix] Google Authenticator (iOS) is able to scan the QR code now

[1.7.0]
* Update Uptime Kuma to 1.9.1
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.9.0)
* Add custom HTTP methods, body and headers (Thanks @bertyhell)
* Add an ability to clear old monitor data automatically (Thanks @chakflying)
* Add Steam Game Server Monitor Type (Thanks @Revyn112)
* [SMTP] Add custom Email subject/title (Thanks @NixNotCastey)
* Add PM2 config (Thanks @GhostSlayer)
* Notifications: Feishu (Thanks @xjoker), Aliyun SMS (Thanks @xjoker), DingDing (Thanks @xjoker)

[1.7.1]
* Update Uptime Kuma to 1.9.2
* Fix 2FA high severity vulnerability bug #833

[1.7.2]
* Update Uptime Kuma to 1.10.0
* Add an ability to change the ping chart period (Max: 1 week) #752 (Thanks @chakflying)
* Certificate expiration notifications #781 (21, 14, 7 days before expires)
* Shrink database size button #794
* Fix SMTP Ignore TLS not working #757
* Fix pagination count by removing it #809 (Thanks @Saibamen)
* Fix logout problem
* Add login rate limit
* 2FA Hardening (#640 Part 1, 2) (Thanks @andreasbrett)
* Improve English grammar (Thanks @NeuralMiner)
* Show logged in username #772 (Thanks @andreasbrett)
* Do not accept weak passwords

[1.7.3]
* Update Uptime Kuma to 1.10.1
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.10.1)
* Display Monitor Tags on Status Page #815 (Thanks @Fallstop)
* Fix monitor could get stuck. More details: #736
* Show tooltip on uptime pill in the monitor list #883 (Thanks @andreasbrett)

[1.7.4]
* Update Uptime Kuma to 1.10.2
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.10.2)
* Fix setting page not working when disabled auth.

[1.8.0]
* Update Uptime Kuma to 1.11.0
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.11.0)
* Redesign Settings page #753 (Thanks @chakflying)
* Add SerwerSMS.pl notification provider #963 (Thanks @kffl)
* Add Stackfield notification provider # (Thanks @jlbrt)
* Basic Auth #864 (Thanks @ivanbratovic)
* Ability to remove 2FA via npm run remove-2fa #933
* [Status Page] Add a new group at the top #884 (Thanks @thomasleveil)
* Improve js bundle size #966
* Fix Pushover #971
* Fix no down notification for push type monitor #922
* Fix npm package vulnerabilities (Thanks @BCsabaEngine)

[1.8.1]
* Update Uptime Kuma to 1.11.1
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.11.1)
* Fixed Safari issue

[1.8.2]
* Update Uptime Kuma to 1.11.2
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.11.2)
* Update to base image 3.2.0
* Added Google Chat Notification #1045 (Thanks @bilipp)
* Added WeCom Notification #1095 (Thanks @LeslieLeung)
* Added SMTP DKIM settings #1080 (Thanks @chakflying)
* Added a new language: Slovenscina (Thanks @drodmantras)
* Fixed unexpected browsers auto completing in the HTTP Basic Auth form #1063 (Thanks @ivanbratovic)
* Improved rare cases that websites only accept text/html but not */* #1067

[1.8.3]
* Update Uptime Kuma to 1.11.3
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.11.3)
* Fixed Google Chat notification issue (Thanks @bilipp)
* Fixed SMTP DKIM crash the server

[1.8.4]
* Update Uptime Kuma to 1.11.4
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.11.4)
* Update vulnerable packages
* ping is supported for all types of BSD #1155 (Thanks @ledeuns)
* Fixed #1024 (Thanks @jbenguira)
* Support password manager filling #1184 (Thanks @Khord)

[1.9.0]
* Update Uptime Kuma to 1.12.1
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.12.1)
* Fixed mattermost couldn't find channel issue #1298 (Thanks @ananthkamath)
* Added a new language: Cestina (Thanks @Buchtic)
* Update axios to 0.26.0 due to vulnerability

[1.10.0]
* Update Uptime Kuma to 1.13.1
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.13.1)
* Initial support for multiple status pages
* Show down count on favicon
* New status page edit mode

[1.10.1]
* Update Uptime Kuma to 1.13.2
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.13.2)

[1.11.0]
* Update Uptime Kuma to 1.14.0
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.14.0)
* Custom domain names for status pages
* Built-in Cloudflare Tunnel supports, a super easy-to-use reverse proxy #1427 Read more...
* Proxy settings for HTTP(s) Monitors #840 (Thanks @ugurerkan)
* Status Page Favicon #1435 (Thanks @LoaderB0T)
* Fix: Remove prometheus metrics on delete #1136
* Add new language: Ukrainian (Thanks @Deni7)
* Show page not found for invalid pages
* Avoid some runtime deprecation warnings #1348 (Thanks @AnnAngela)
* Fix Alerta issue (Thanks @Arubinu)
* Fix Mattermost issue when the channel field is empty

[1.11.1]
* Allow to set domain aliases for status dashboards

[1.11.2]
* Update Uptime Kuma to 1.14.1
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.14.1)
* Fix Uptime Kuma 1.14.0 crashed at night (#1520)

[1.12.0]
* Update Uptime Kuma to 1.15.0
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.15.0)
* [Status Page] Custom CSS (Thanks @patrickhafner)
* [Status Page] Footer Text
* [Status Page] Show/Hide Powered by Uptime Kuma

[1.12.1]
* Update Uptime Kuma to 1.15.1
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.15.1)
* Fix settings cannot be saved in "No Auth" mode #1577
* Add the ability to specify status in the push URL (e.g. /api/push/<TOKEN>?status=down&msg=OMG) (Thanks @trogper)

[1.13.0]
* Update Uptime Kuma to 1.16.0
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.16.0)
* Add badges API (Wiki will be added later, please read #1119) (Thanks @jensneuber) image
* Add optional title for Apprise (Fix apprise integration for Zulip Streams) (Thanks @c-w)
* Add a new notification: PagerDuty (Thanks @MarcHagen)
* Allow to specify Resolver Port for DNS Monitor (Thanks @Computroniks)

[1.13.1]
* Update Uptime Kuma to 1.16.1
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.16.1)
* Fix tags that do not show on status pages
* Synchronize push monitor heartbeat, fix race condition (Thanks @kaysond)

[1.14.0]
* Update Uptime Kuma to 1.17.0
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.17.0)
* Add settings for notification of certificate expiry (Thanks @chakflying)
* Cache DNS records on HTTP(s) monitor (Thanks @gregdev)
* Add new monitor type: MS SQL Server (Thanks @christopherpickering)
* Add new notification: ntfy (Thanks @philippdormann)
* Add NTML Auth Option for HTTP(s) (Thanks @christopherpickering)
* Pre-render some html for status page such as title and meta tag
* Optimize page load time by Gzip or Brotli

[1.14.1]
* Update Uptime Kuma to 1.17.1
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.17.1)
* Fix: Revert Caching DNS records on HTTP(s) monitor #1821

[1.14.2]
* Source custom env vars from `/app/data/env`

[1.15.0]
* Update Uptime Kuma to 1.18.0
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.18.0)
* Add new monitor type: Docker container (Thanks @c0derMo)
* Add new monitor type: PostgreSQL server (Thanks @christopherpickering)
* Add new monitor type: Radius (Thanks @knopwob)
* Add ability to resend notification if down X times consequently (Thanks @OidaTiftla)
* [Status Page] Ability to set clickable link for https/http monitors (Thanks @Computroniks)
* Trust Proxy and read X-Forwarded headers (Thanks @harryzcy, @theS1LV3R, @mhajder)
* Add Basque language (Thanks @utolosa002)
* Add new notification: AlertNow (Thanks @daeho-ro)
* Add new notification: Line Notify (Thanks @0x01code)
* Add new notification: Home Assistant notification (Thanks @rmtsrc)
* Add new language: Português (Portugal) (pt-PT) (Thanks @mariogarridopt)
* Re-introduce DNS caching for http/https monitor (Using cacheable-lookup which is more popular)
* Hide mobile footer when not logged in (Thanks @treboryx)
* Decrease SQLite query timeout from default to 5 seconds (Thanks @jbenguira)
* Add more settings for Bark Notification (Thanks @SuperManito)
* Update Apprise from 0.9.9 to 1.0.0 (Release Notes)
* Add label to status badge (Thanks @woooferz)

[1.15.1]
* Update Uptime Kuma to 1.18.1
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.18.1)
* Add GoAlert Notification (Thanks @mhkarimi1383)
* Add SMSManager Notification (Thanks @jakubenglicky)
* Add ServerChan Notification (Thanks @mjysci)
* Add Squadcast Notification (Thanks @phindmarsh)
* Fix Bark (Thanks @zuosc @SuperManito)
* Fix Octopush and improve error handling (Thanks @Computroniks)
* Add authentication support for ntfy (Thanks @rolfbachmann)
* Improve support for password managers (Thanks @ThomasChr)
* Add Cypress for e2e testing (Thanks @tamasmagyar)

[1.15.2]
* Update Uptime Kuma to 1.18.2
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.18.2)

[1.15.3]
* Update Uptime Kuma to 1.18.3
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.18.3)
* Add free.fr SMS notification (Thanks @CL0Pinette)
* Fix ntfy issue in 1.18.1 (If you are using ntfy, you should update the instance to 1.18.3)
* Fix issue when multiple IPs in X-Forwarded-For (Thanks @benscobie)
* Hide console window when using ping monitor type on Windows (Thanks @Sympatron)

[1.15.4]
* Update Uptime Kuma to 1.18.4
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.18.4)
* Fix ntfy issue in 1.18.1 again, username is no longer required
* Add Greek Language (Thanks @VasilisThePikachu)

[1.15.5]
* Update Uptime Kuma to 1.18.5
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.18.5)
* Fix misused of jesec in 1.18.4 (Which causes some status page features are broken such as custom css)

[1.15.6]
* Cleanup backup sqlite database files on restart

[1.16.0]
* Update Uptime Kuma to 1.19.0
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.19.0)
* **Since server side timezone is configurable now, you should check whether it is correct in the Settings.**
* **As current DNS cache mechanism is not working properly in some IPv6 networks, DNS cache is now turned off by default. Turn this back on manually in Settings page if you need this.**
* Add gRPC monitor (Thanks @minhhoangvn)
* Add MySQL monitor (Thanks @Computroniks)
* Add socks5h support (Thanks @jbrunner)
* Add SMSEagle Notification (Thanks @m-kiszka)
* [Ntfy] Add ability to add icon (Thanks @cmeis)
* Add configurable server timezone
* Log time is now using the server timezone instead of UTC time
* Add additional headers option for webhook notifications (Thanks @janhartje)
* Fixed entry route not redirecting correctly (Thanks @UltraWelfare)
* Add support for custom radius ports (Thanks @Computroniks)
* Enable/Disable DNS cache in Settings
* Update SQLite to 3.39.4
* Fix 'undefined' in link preview generation (Thanks @Dafnik)
* Fix sometimes monitor reports unexpected error: "maxContentLength size of -1 exceeded"
* Add new language German (Switzerland) (Thanks @saw303)
* Improve Docker healthcheck script
* Cache uptime values

[1.16.1]
* Update Uptime Kuma to 1.19.2
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.19.2)
* Fix the UI broken after removed a monitor

[1.16.2]
* Update Uptime Kuma to 1.19.3
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.19.3)
* Add Hebrew translation (Thanks @thefourcraft)
* Add Zoho Cliq notification (Thanks @pbaris)
* Add Kook notification (Thanks @YehowahLiu)
* Fix MS SQL monitors interfere with each other
* Fix gRPC check throws errors when response data size > 50 chars (Thanks @mathiash98)
* Fix Slack notification do not show message correctly (Thanks @pruekk)

[1.16.3]
* Update Uptime Kuma to 1.19.4
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.19.4)
* #2527 Fix: [Docker Monitor] Incorrect handling for container down (Thanks @chakflying)
* #2528 Fix: Add tag issue (Thanks @chakflying)
* #2569 Fix: Add validation of Keep Monitor History Days (Thanks @Computroniks)
* #2223 Improvement: Better support for ping monitor on different Linux distros (Thanks @Computroniks)
* #2543 Improvement: Remove redundant title in Pushover notification (Thanks @SlothCroissant)

[1.16.4]
* Enable trust proxy by default
* Disable update check

[1.16.5]
* Update Uptime Kuma to 1.19.5
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.19.5)
* #2575 Add: Splunk Notification Provider (Thanks @Joseph-Irving)
* Add: a Help button which links to the wiki
* Fix: Maintenance count is incorrect
* Fix: Ping monitor issues after 1.9.4
* Fix: Postgres monitor do not handle some error cases correctly
* #2540 Fix: Allow more MQTT protocols (mqtt, mqtts, ws and wss) for MQTT monitor (Thanks @twiggotronix)
* #2586 Fix: PromoSMS notification is not sent if the message is too long (Thanks @PopcornPanda)
* Fix: Security patch from upstream dependencies

[1.16.6]
* Update Uptime Kuma to 1.19.6
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.19.6)

[1.17.0]
* Update Uptime Kuma to 1.20.0
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.20.0)
* #1690 Tags Manager (Thanks @chakflying)
* #2566 GameDig monitor, now you can monitor a lot of game servers via this monitor, check this game list here. (Thanks @WhyKickAmooCow)
* #2541 Redis monitor (Thanks @long2ice)
* #2328 MongoDB monitor (Thanks @rmarops)
* #2211 More badges (See here) (Thanks @JRedOW)
* #1892 [Ping Monitor] Configurable packet size (Thanks @Computroniks)
* #2570 [Status Page] Markdown support (Thanks @Computroniks)

[1.17.1]
* Update Uptime Kuma to 1.20.1
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.20.1)
* #2779 Database deleted unexpectedly if disk space is not enough when updating (Thanks @Computroniks)
* d1175ff Postgres monitor crashed if the sql query statement was not provided lizard Translation Contributors
* #2770 New language: Finnish (Thanks @JonneSaloranta)
* #2767 @victorpahuus @denyskon @lfac76 @Genc @eltionb @JonneSaloranta @AmadeusGraves @MrEddX @black23 darkslash#2558

[1.17.2]
* Update Uptime Kuma to 1.20.2
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.20.2)
* #2820 Fix memory leak issue from MongoDB driver (Thanks @Lanhild @chakflying)
* #2083 Recompile healthcheck.go. Forgot to compile it in 1.20.0, hence the issue is still presented in 1.20.0.

[1.18.0]
* Update Uptime Kuma to 1.21.0
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.21.0)
* #2489 Ability to clone monitors (Thanks @mathiash98 @chakflying)
* #1685 API Keys Management (Currently, it is used for Prometheus metrics API only) (Thansk @Computroniks)
* #2806 [Status Page] Add markdown support for description (Thanks @andreasbrett)
* #2531 [HTTP(s) Monitor] New Auth method: mTLS
* #2610 [HTTP(s) Monitor] Support XML Request Body (Thanks @bayramberkay @Genc @JustinTisdale)
* #2764 Add "Add New Tag" button in Settings (Thanks @chakflying)
* #939 Ability to set description of monitors (Thanks @jcvincenti)
* #2728 PagerTree Notification Provider (Thanks @armiiller)
* Update Apprise to 1.3.0
* #2191 Windows portable executable (uptime-kuma.exe) (Support Windows x64 only) (See uptime-kuma-win64-portable-1.0.0.zip below)

[1.18.1]
* Update Uptime Kuma to 1.21.1
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.21.1)
* #2956 New notification provider: Opsgenie (Thanks @wwniclask25)
* #2980 New notification provider: Twilio Sms (Thanks @Genc)
* #2970 MySQL monitor crash issue
* #2969 A cloned push type monitor that do not generate a new push token
* #2958 The copy text button is working in the API key dialog
* #2906 Fix: Check for TLS expiry notified days smaller than target (Thanks @chakflying)
* #2975 Fix: Allow status badge with empty label (Thanks @chakflying)
* #2973 Fix rounding issue in badges (Thanks @chakflying)
* #2878 Show a refresh countdown in the bottom of status pages (Thanks @chakflying)
* #2908 Ability to set the ssl key passphrase (Thanks @chakflying)

[1.18.2]
* Update Uptime Kuma to 1.21.2
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.21.2)
* Now you can schedule maintenance with a cron expression + duration
* #3003 Maintenance is completely bugged in 1.21.1 and 1.21.0
* #2988 Fix Ignore TLS/SSL Error is not working when using a proxy (Thanks @chakflying)

[1.18.3]
* Update Uptime Kuma to 1.21.3
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.21.3)
* [Maintenance] Timezone issues that causes some maintenance do not start on time
* [Maintenance] Same as Server Timezone cannot be saved

[1.19.0]
* Update Uptime Kuma to 1.22.0
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.22.0)
* #2693 #3270 #3246 #3311 Monitor group (You could find it under Monitor Type -> Group) (Thanks @julian-piehl @chakflying @tarun7singh)
* #2915 Badge generator (You could find it in status pages) (Thanks @kiznick)
* #2870 Support auto theme in status pages (Thanks @chakflying)
* #2905 Add option for notification provider ntfy to use access tokens (Thanks @Sharknoon)
* #2752 [Status Page] Show tags in the monitor select list (Thanks @titanventura)
* #2491 The metrics issue for the push type (Thanks @RubenNL)
* #2867 #3010 Invalid gRPC URL issue (Thanks @Small6oy @chakflying)
* #3222 Try to close mysql connection properly (Thanks @chakflying)
* #2831 Improve Mattermost notifications (Thanks @mtelgkamp)
* #2863 Improve display messages for Ntfy notifications (Thanks @mtelgkamp)
* #2837 Allow hostnames with a trailing dot (Thanks @dsb3)
* #2868 Update chart.js & improve performance (Thanks @chakflying)
* #3006 Improve the tags settings design on mobile (Thanks @chakflying)
* #3052 Improve the monitor details page on mobile (Thanks @chakflying)
* #3024 Improve Edit Tag (Thanks @chakflying)
* #3054 [Keyword monitor] Trim before truncating "keword not found" message (Thanks @TechWilk)
* #3154 Improve memory usage of background jobs (Thanks @chakflying)
* #3009 Display hostname or url for more monitor types (Thanks @chakflying)
* #2916 Improve navbar on iPhone (Thanks @lukasbableck)
* #3156 [Pushover notification] Add the ability to specific the message TTL (Thanks @maximilian-krauss)
* #2594 Support TLS Expiry alerts also for CA certs in cert chain (Thanks @skaempfe)
* #3203 #3152 All notifications now show the local time instead of UTC (Thanks @CommanderStorm and @AnnAngela)

[1.19.1]
* Update Uptime Kuma to 1.22.1
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.22.1)
* #3347 Uptime Kuma will no longer make requests to check update if you have disabled it. (Thanks @tofran)
* #3296 Fixed auto theme for status pages on custom domains (Thanks @crystalcommunication)
* #3346 Removed all unreleased plugin feature code from Uptime Kuma to eliminate vulnerabilities. (Thanks @n-thumann)

[1.20.0]
* Update Uptime Kuma to 1.23.0
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.23.0)
* #3312 #3469 Added an improved comprehensive monitor list filtering (Thanks @chakflying)
* #1886 Added an ability to bulk select, pause & resume (Thanks @simshaun)
* #2142 [HTTP Monitor] Added an ability to set a request timeout value (Thanks @zenyr)
* #3219 Show elapsed time under the heartbeat bar (Thanks @chakflying)
* #3308 Added new HTTP(s) monitor that uses the Chromium engine (Beta)
* #3253 Added JSON query monitor (Thanks @mhkarimi1383 @CommanderStorm)

[1.20.1]
* Update Uptime Kuma to 1.23.1
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.23.1)
* #3593 Password manager friendly (Thanks @bt90)
* #3614 [DNS monitor] Improve message handling (Thanks @chakflying)
* #3634 [Discord notification] Show hostname:port for gamedig (Thanks @liorsl)

[1.20.2]
* Update Uptime Kuma to 1.23.2
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.23.2)
* #3701 Fixed the issue that Tailscale monitor do not show in the list correctly for non-Docker users (Thanks @ZaneL1u)
* #3717 Fixed the issue that a paused monitor is started after edited (Thanks @chakflying)
* #3729 Fixed the status page items get duplicated when the save button is clicked more than once (Thanks @chakflying)
* #3727 Fixed ths issue that the Oauth2 auth method is not working (Thanks @hegerdes)
* #3751 Added back some missing http options into the json query monitor (Thanks @obfusk)

[1.21.0]
* Update base image to 4.2.0

[1.21.1]
* Update Uptime Kuma to 1.23.3
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.23.3)
* Fixed persistent session tokens issue. There was no way to revoke session tokens even if you changed the password before this update. 
* #3763 Fixed an issue that notification is not working if the config is too long (Thanks @FJBlok)
* #3649 Enable status page certificate expiry badge for all HTTP(s) monitors (Thanks @marvinruder)
* #3771 Fixed kafka producer bugs (Thanks @mhkarimi1383)
* #3726 Fixed an issue that x-forwarded-host is not being used correctly (Thanks @xuexb)
* #3849 Fixed a race condition issue that some data is not being saved in the status page editor if you clicked it too fast (Thanks @chakflying)

[1.21.2]
* Update Uptime Kuma to 1.23.4
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.23.4)
* #3874 Fix: Stop notification check on root certs (Thanks @chakflying)
* #3899 [MySQL Monitor] Fixed a password issue that the connection string do not accept some special characters by adding a standalone password field
* [Postgres / MySQL / MSSQL Monitor] Fixed wrong query examples and the query field is optional now
* #3880 [Postgres Monitor] Fixed an issue that Uptime Kuma crash if your connection string contains ssl=false
* #4011 Fixed timeout issue for old monitors
* #3961 [HTTP Monitor] Fixed an issue that HTTP requests could possibly do not timeout correctly if there is any connection issues

[1.21.3]
* Update Uptime Kuma to 1.23.5
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.23.5)
* #4032 Fixed an issue that paused monitors were being resumed automatically (Thanks @chakflying)
* #4044 Allow legacy TLS renegotion by default again
* #4043 Should be a final fix for the kafka migration script issue
* #4045 Should be a final fix for the timeout issue

[1.21.4]
* Update Uptime Kuma to 1.23.6
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.23.6)
* Fixed an TLS issue that only in Node.js <=16, which is caused by #4044

[1.21.5]
* Update Uptime Kuma to 1.23.7
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.23.7)
* #4053 Show the original timeout message again and +10 seconds for abort signal
* #4054 Improved error message on abort signal timeout (Thanks @chakflying)
* #4084 Fixed a memory leak issue. Close the client postgresql connection after rejection. (Thanks @mvaled)
* #4088 Reverted "Restart running monitors if no heartbeat", which causes some issues from 1.23.4 to 1.23.6.
* f28dccf4e11f041564293e4f407e69ab9ee2277f An XSS issue in the "Google Analytics ID" text field (Reported by @gtg2619) https://github.com/louislam/uptime-kuma/security/advisories/GHSA-v4v2-8h88-65qj
* #4095 Rewrite Tailscale ping using spawnSync (Reported by @vaadata-pascala) https://github.com/louislam/uptime-kuma/security/advisories/GHSA-hfxh-rjv7-2369
* b689733d59139f158de95b0017865c867d0c7159 getGameList, testChrome are no longer accessible without login

[1.21.6]
* Update Uptime Kuma to 1.23.8
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.23.8)
* #4139 Default Retries values from 1 to 0
* #4132 #4133 Improved accessibility (Thanks @CommanderStorm)
* #4141 Added support for /snap/bin/chromium (Ubuntu's default Chromium path)
* #4123 Fixed an issue that Tailscale monitor could freeze Uptime Kuma, which is caused by the last Tailscale monitor security fix.
* #4136 Updated gamedig from ~4.1.0 to ^4.2.0
* #4140 Updated apprise from 1.4.5 to 1.6.0

[1.21.7]
* Update Uptime Kuma to 1.23.9
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.23.9)
* #4163 Add an aria-label to the monitor search box (Thanks @CommanderStorm)
* #4175 chore: added a helptext for ntfy's priority field (Thanks @CommanderStorm)
* #4186 Fix: Correct Maintenance Start/End Time Input to Use Explicitly Specified Timezone (Thanks @Ritik0102)
* #4162 Fixed the buttons of ActionsSelect and ActionsInput having a default type="submit" (Thanks @CommanderStorm)
* https://github.com/louislam/uptime-kuma/security/advisories/GHSA-88j4-pcx8-q4q3
* https://github.com/louislam/uptime-kuma/security/advisories/GHSA-mj22-23ff-2hrr
* Added an environment variable `UPTIME_KUMA_WS_ORIGIN_CHECK`: cors-like (default) and bypass

[1.21.8]
* Wait for db to initialize using healthcheck instead of sleeping 10 seconds

[1.21.9]
* Update Uptime Kuma to 1.23.10
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.23.10)
* #4216 Due to a security patch in 1.23.9, some Uptime Kuma setups behind a reverse proxy required re-configuration. It is no longer necessary now.
* #4213 GHSA-88j4-pcx8-q4q3 was not fully patched due to a careless mistake (Thanks @chakflying)

[1.21.10]
* Update Uptime Kuma to 1.23.11
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.23.11)
* #4224 Fix: origin undefined on error handling (Thanks @chakflying)
* #4231 [Non-docker] Fixed the Tailscale monitor issue which was broken in 1.23.8

[1.21.11]
* Update Uptime Kuma to 1.23.12
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.23.12)
* #4477 Improved helptext of how to send mail via the systems mail subsystem (Thanks @apio-sys)
* #4630 Feat: Use keylog event to obtain TLS certificate for better reliability [1.23.X] (Thanks @chakflying)
* #4326 Fix encodeBase64 for empty password or user in HTTP Basic Authentication (Thanks @Saibamen)
* #4244 made sure that the i18n does use navigator.languages instead of navigator.language for automatic language detection (Thanks @CommanderStorm)
* #3598 fix(notification-dingding): throw error when failed (Thanks @AnnAngela)
* #4417 Fix: Make sure browser is connected before returning (Thanks @chakflying)
* #4425 Fix: [JSON-Query] Prevent parsing string-only JSON (Thanks @chakflying)
* #4631 Fix: Add missing FK for monitor-tls-info table [1.23.X] (Thanks @chakflying)

[1.21.12]
* Update Uptime Kuma to 1.23.13
* [Full changelog](https://github.com/louislam/uptime-kuma/releases/tag/1.23.13)
* #4692 Fixed language setting issues: Localisation-matching algorithm missing some edgecase (Thanks @CommanderStorm)
* #4700 Fixed TLS issues: Getting TLS certificate through proxy & prometheus update (Thanks @chakflying)

