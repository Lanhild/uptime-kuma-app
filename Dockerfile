FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

ARG NODE_VERSION=16.14.2
RUN mkdir -p /usr/local/node-$NODE_VERSION && \
    curl -L https://nodejs.org/dist/v$NODE_VERSION/node-v$NODE_VERSION-linux-x64.tar.gz | tar zxf - --strip-components 1 -C /usr/local/node-$NODE_VERSION
ENV PATH /usr/local/node-$NODE_VERSION/bin:$PATH

# https://github.com/cloudflare/cloudflared/releases
ARG CLOUDFLARED_VERSION=2023.10.0

RUN curl -L https://github.com/cloudflare/cloudflared/releases/download/${CLOUDFLARED_VERSION}/cloudflared-linux-amd64 -o /usr/bin/cloudflared && \
    chmod +x /usr/bin/cloudflared

ARG VERSION=1.23.13

RUN curl -L https://github.com/louislam/uptime-kuma/archive/refs/tags/${VERSION}.tar.gz | tar zx --strip-components 1 -C /app/code

RUN npm i && npm run build && npm cache clean --force

COPY start.sh /app/pkg

RUN rm -rf /app/code/data && ln -s /app/data/data /app/code/data

CMD [ "/app/pkg/start.sh"]
