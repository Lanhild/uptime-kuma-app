## About

It is a self-hosted monitoring tool like "Uptime Robot".
 
## Feature

* Monitoring uptime for HTTP(s) / TCP / Ping.
* Fancy, Reactive, Fast UI/UX.
* Notifications via Webhook, Telegram, Discord and email (SMTP).
* 20 seconds interval.

