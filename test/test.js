#!/usr/bin/env node

/* jshint esversion: 8 */
/* jslint node:true */
/* global it:false */
/* global xit:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = 'test';
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 50000;

    let browser, app;
    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;

    before(function () {
        browser = new Builder().forBrowser('chrome').setChromeOptions(new Options().windowSize({ width: 1280, height: 1024 })).build();
    });

    after(function () {
        browser.quit();
    });

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    }

    async function setup() {
        // going to /setup directly has issues - https://github.com/louislam/uptime-kuma/issues/159
        await browser.get(`https://${app.fqdn}`);

        await browser.wait(until.elementLocated(By.xpath('//button[contains(text(), "Create")]')), TIMEOUT);
        await browser.findElement(By.xpath('//input[@placeholder="Username"]')).sendKeys(username);
        await browser.findElement(By.xpath('//input[@placeholder="Password"]')).sendKeys(password);
        await browser.findElement(By.xpath('//input[@placeholder="Repeat Password"]')).sendKeys(password);

        const button = await browser.findElement(By.xpath('//button[contains(text(), "Create")]'));
        await browser.executeScript('arguments[0].scrollIntoView(true)', button);
        await browser.sleep(3000);
 
        await browser.findElement(By.xpath('//button[contains(text(), "Create")]')).click();

        await browser.wait(async function () {
            const url = await browser.getCurrentUrl();
            return url === `https://${app.fqdn}/dashboard`;
        });
    }

    async function login() {
        await browser.manage().deleteAllCookies();

        await browser.get('https://' + app.fqdn);
        await browser.executeScript('localStorage.clear();');

        await browser.wait(until.elementLocated(By.xpath('//button[contains(text(),"Login")]')), TIMEOUT);
        await browser.findElement(By.xpath('//input[@placeholder="Username"]')).sendKeys(username);
        await browser.findElement(By.xpath('//input[@placeholder="Password"]')).sendKeys(password);

        await browser.findElement(By.xpath('//button[contains(text(),"Login")]')).click();

        await browser.wait(until.elementLocated(By.xpath('//h1[contains(text(), "Quick Stats")]')), TIMEOUT);
    }

    async function addMonitor() {
        await browser.get('https://' + app.fqdn + '/add');
        await browser.wait(until.elementLocated(By.xpath('//h1[text()="Add New Monitor"]')), TIMEOUT);

        await browser.findElement(By.xpath('//input[@id="name"]')).sendKeys('Cloudron Website');
        await browser.findElement(By.xpath('//input[@id="url"]')).sendKeys('cloudron.io');
        const button = await browser.findElement(By.xpath('//button[@type="submit"]'));
        await browser.executeScript('arguments[0].scrollIntoView(true)', button);
        await browser.sleep(5000);
        await button.click();
        await browser.wait(until.elementLocated(By.xpath('//h1[text()="Cloudron Website"]')), TIMEOUT);
    }

    async function checkMonitor() {
        await browser.get('https://' + app.fqdn + '/dashboard/1');
        await browser.wait(until.elementLocated(By.xpath('//h1[text()="Cloudron Website"]')), TIMEOUT);
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', async function () {
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        await browser.sleep(10000);
    });

    it('can get app information', getAppInfo);

    it('can setup', setup);
    it('add monitor', addMonitor);
    it('check monitor', checkMonitor);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });
    it('check existing monitor', checkMonitor);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', async function () {
        await browser.get('about:blank');
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('check existing monitor', checkMonitor);
    it('move to different location', async function () {
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
    });
    it('can get app information', getAppInfo);
    it('can login', login);
    it('check existing monitor', checkMonitor);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app', async function () {
        execSync(`cloudron install --appstore-id louislam.uptimekuma.app --location ${LOCATION}`, EXEC_ARGS);
        await browser.sleep(10000);
    });
    it('can get app information', getAppInfo);
    it('can setup', setup);
    it('add monitor', addMonitor);
    it('check monitor', checkMonitor);

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });
    it('check existing monitor', checkMonitor);
    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});

